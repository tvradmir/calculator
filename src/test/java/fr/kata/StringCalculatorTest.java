package fr.kata;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class StringCalculatorTest {

    @Test
    void should_return_zero_when_add_empty_string()  {
        int sum = StringCalculator.add("");
        assertEquals(0,sum);
    }

    @Test
    void should_return_one_when_add_string_of_one()  {
        assertEquals(1,StringCalculator.add("1"));
    }

    @Test
    void should_return_six_when_add_string_of_six()  {
        assertEquals(6,StringCalculator.add("6"));
    }

    @Test
    void should_return_twenty_when_add_string_of_six()  {
        assertEquals(6,StringCalculator.add("6"));
    }


    @Test
    void should_return_three_when_add_string_of_one_and_two_separated_by_comma()  {
        assertEquals(3,StringCalculator.add("1,2"));
    }


    @Test
    void should_return_twenty_two_when_add_string_of_ten_and_twelve_separated_by_comma()  {
        assertEquals(22,StringCalculator.add("10,12"));
    }

    @Test
    void should_return_six_when_add_string_of_one_two_and_three_separated_by_comma()  {
        assertEquals(6,StringCalculator.add("1,2,3"));
    }

    @Test
    void should_return_forty_five_when_add_string_of_ten_thirteen_and_twelve_separated_by_comma()  {
        assertEquals(35,StringCalculator.add("10,13,12"));
    }

    @Test
    void should_return_six_when_add_string_of_one_two_new_line_comma_three()  {
        assertEquals(6,StringCalculator.add("1\n2,3"));
    }


    @Test
    void should_return_six_when_add_string_of_one_new_line_comma_two()  {
        assertEquals(3,StringCalculator.add("1\n2"));
    }

    @Test
    void should_return_three_when_add_string_of_one_and_two_separated_by_custom_delimiter_as_semicolon()  {
        assertEquals(3,StringCalculator.add("//;\n1;2"));
    }

    @Test
    void should_return_six_when_add_string_of_one_two_and_three_separated_by_custom_delimiter_as_semicolon()  {
        assertEquals(6,StringCalculator.add("//;\n1;2;3"));
    }

    @Test
    void should_return_six_when_add_string_of_one_two_and_three_separated_by_custom_delimiter_as_percent_sign()  {
        assertEquals(3,StringCalculator.add("//%\n1%2"));
    }

    @Test
    void should_return_six_when_add_string_of_one_two_and_three_separated_by_custom_delimiter_as_two_semicolon()  {
        assertEquals(6,StringCalculator.add("//;;\n1;;2;;3"));
    }

    @Test
    void should_throws_exception_with_message_negatives_not_allowed_when_add_string_of_minus_one()  {
        assertThatThrownBy(()->StringCalculator.add("-1")).isInstanceOf(IllegalArgumentException.class).hasMessage("negatives not allowed: %s","-1");
    }

    @Test
    void should_throws_exception_with_message_negatives_not_allowed_when_add_string_of_minus_one_two_minus_three_and_five_separated_with_comma_()  {
        assertThatThrownBy(()->StringCalculator.add("-1,2,-3,5")).isInstanceOf(IllegalArgumentException.class).hasMessage("negatives not allowed: %s","-1,-3");
    }

    @Test
    void should_return_two_when_add_string_of_two_and_thousand()  {
        assertEquals(1002,StringCalculator.add("2,1000"));
    }

    @Test
    void should_return_two_when_add_string_of_two_and_thousand_one()  {
        assertEquals(2,StringCalculator.add("2,1001"));
    }

    @Test
    void should_return_zero_when_add_string_of_two_thousand_and_thousand_one()  {
        assertEquals(0,StringCalculator.add("2000,1001"));
    }

    @Test
    void should_return_six_when_add_string_of_one_two_and_three_delimiter_of_triple_asterisk()  {
        assertEquals(6,StringCalculator.add("//[***]\n1***2***3"));
    }

    @Test
    void should_return_six_when_add_string_of_one_two_and_three_delimiter_of_triple_percent_sign()  {
        assertEquals(6,StringCalculator.add("//[%%%]\n1%%%2%%%3"));
    }

    @Test
    void should_return_six_when_add_string_of_one_two_and_three_delimiters_of_asterisk_percent_sign()  {
        assertEquals(6,StringCalculator.add("//[*][%]\n1*2%3"));
    }

    @Test
    void should_return_ten_when_add_string_of_one_two_and_three_four_delimiters_of_asterisk_percent_sign_semicolon()  {
        assertEquals(10,StringCalculator.add("//[*][%][;]\n1*2%3;4"));
    }

    @Test
    void should_return_ten_when_add_string_of_one_two_and_three_four_delimiters_of_three_asterisks_three_percent_signs()  {
        assertEquals(6,StringCalculator.add("//[***][%%%]\n1***2%%%3"));
    }

    @Test
    void should_return_ten_when_add_string_of_one_two_and_three_four_delimiters_of_three_asterisks_three_percent_signs_three_semicolon()  {
        assertEquals(6,StringCalculator.add("//[***][%%%][;;;]\n1***2%%%3"));
    }




}
