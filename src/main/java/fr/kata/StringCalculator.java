package fr.kata;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StringCalculator {

    public static final String DEFAULT_SEPARATOR = ",|\n";
    public static final String NEW_LINE = "\n";
    public static final String CUSTOM_PREFIX = "//";
    public static final String NEGATIVES_NOT_ALLOWED_EXCEPTION_MESSAGE = "negatives not allowed: ";
    public static final String OPEN_CROCHET = "[";
    public static final String CLOSE_CROCHET = "]";
    public static final String PLUS_SIGN = "+";
    public static final String REGEX_OR = "|";

    public static int add(String input) {
        String separators = buildSeparators(input);

        String numbers= extractNumbers(input);

        verifyIfContainsNegativeNumbers(numbers, separators);

        return getSum(numbers,separators);

    }

    private static String buildSeparators(String input) {
        StringBuilder separatorsBuilder = new StringBuilder(OPEN_CROCHET +DEFAULT_SEPARATOR);
        if(hasCustomDelimiter(input)) {
            String customDelimiter = extractCustomDelimiter(input, input.indexOf(NEW_LINE));
            addCustomDelimiter(customDelimiter, separatorsBuilder);
        }
        separatorsBuilder.append(CLOSE_CROCHET + PLUS_SIGN);
        return separatorsBuilder.toString();
    }

    private static void addCustomDelimiter(String delimiters, StringBuilder stringBuilder) {
        stringBuilder.append(REGEX_OR);
        stringBuilder.append(delimiters);
    }

    private static String extractNumbers(String numbers) {
        if(hasCustomDelimiter(numbers)){
            return numbers.substring(numbers.indexOf(NEW_LINE)+1);
        }else{
            return numbers;
        }
    }

    private static String extractCustomDelimiter(String numbers, int newLineIndex) {
        return numbers.substring(numbers.indexOf(CUSTOM_PREFIX)+2,newLineIndex);
    }

    private static boolean hasCustomDelimiter(String numbers) {
        return numbers.startsWith(CUSTOM_PREFIX);
    }

    private static int getSum(String inputNumbers, String separators) {
        return getValidateIntStreamNumbers(inputNumbers, separators).sum();
    }

    private static void verifyIfContainsNegativeNumbers(String inputNumbers, String separators) {
        String negativeNumbers = getValidateIntStreamNumbers(inputNumbers, separators).filter(number -> number < 0).mapToObj(Integer::toString).collect(Collectors.joining(","));
        if(!isEmpty(negativeNumbers)){
          throw new IllegalArgumentException(NEGATIVES_NOT_ALLOWED_EXCEPTION_MESSAGE +negativeNumbers);
        }
    }

    private static boolean isEmpty(String negativeNumbers) {
        return negativeNumbers.isBlank();
    }

    private static IntStream getValidateIntStreamNumbers(String inputNumbers, String separators) {
        if(isEmpty(inputNumbers)){
            return IntStream.empty();
        }
        return Pattern.compile(separators).splitAsStream(inputNumbers).mapToInt(StringCalculator::convertStringNumberToInt).filter(number->number<=1000);
    }

    private static int convertStringNumberToInt(String number) {
        return Integer.parseInt(number);
    }
}
